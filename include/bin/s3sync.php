#!/usr/bin/php
<?php
// spec: Mirror a set of directories to S3

require_once('/opt/edoceo/lib/libs3.php');

register_shutdown_function('local_shutdown_function');
clearstatcache();

$params = array();
$params['debug_trace'] = false;
$params['debug_curl'] = false;
$params['debug_curl_callback'] = false;
$params['debug_s3'] = false;

/*
todo: function like this dummy prototype
for file in `find /var/rdiff-backup`; do
  if (archive_database_exists($file))
	{
		if (!still_in_system($file)) { remove_from_archive($file); }
		else { compare_checksums($file); }
	}
	else
	{
		push_to_archive($file);
		archive_database_add($file):
	}
done

#!/bin/bash
# To Encrypt

#MCRYPT_ALGO
#MCRYPT_KEY="61dce73cd36c7f721d798ac12ec54fd968cfeb90e38cd2be9910ba1b25ed46bdef1fddc9cc98cc44d4431ca99d447"
#MCRYPT_KEY_MODE=s2k-salted-sha1
#MCRYPT_MODE

mcrypt --algorithm rijndael-256 --hash sha256 --keymode s2k-salted-sha1 --mode cbc --openpgp
mcrypt --algorithm rijndael-256 --gzip --hash sha256 --keymode s2k-salted-sha1 --mode cbc
*/

// s3sync_cache_update($o_bucket,$o_prefix);
$o_source = $argv[1];
$o_prefix = $argv[2];

$s3c = new S3Connection($key,$secret,$params);
$bucket = $s3c->path_split($o_prefix,'bucket');

$s3cache = new S3Cache("/opt/edoceo/var/s3sync/$bucket.cache");

echo "Cache Size:...\n";

// now read the directory to be mirrored
$dir_list = array(); // Local List
list_directory($o_source);
echo sprintf("There are %d local entries\n",count($dir_list));
sleep(1);

$put_objects = 0;
foreach ($dir_list as $file_name=>$file_data)
{
	$s3path = $s3c->path_fix($o_prefix.$file_name);
	echo "S3Key: $s3path\n";

	$action = null;

	// Check Local Cache
	$meta_data = $s3cache->hit($s3path);
	// make sure these values are complete
	if (!isset($meta_data['stat_hash']))
	{
		echo "Cache Miss\n";
		//print_r($meta_data);

		$s3cache->add($s3path,$file_data);
		$s3r = $s3c->stat($s3path);
		if ($s3r->is_success) $meta_data = $s3r->meta;
		//print_r($meta_data);
	}

	// Compare (Local Cache | S3Stat) to LocalData
	if ($file_data['stat_hash'] != $meta_data['stat_hash'])
	{
		echo "Checking MD5s\n";
		$md5 = null;
		if (is_link($file_name)) $md5 = 'd41d8cd98f00b204e9800998ecf8427e';
		elseif (is_file($file_name)) $md5 = md5_file($file_name);
		else die("Can't MD5: ".$file_name);

		if ("\"$md5\"" != $s3r->head_etag) $action = 'put';
	}

	// Have to put it
	if ($action=='put')
	{
		echo "\n";
		echo "Putting $file_name to $s3path\n";
		$s3r = $s3c->put($file_name,$s3path,$file_data);
		if (!$s3r->is_success) die("Error: $s3r->as_string");
		$s3r = $s3c->stat($s3path);
		$s3cache->add($s3path,$s3r->meta);
		$put_objects++;
		if ($put_objects > 200) exit;
	}
}

exit;

// func: list_directory($dir) - build a list of files
function list_directory($dir)
{
	global $dir_list;
	if (substr($dir,-1) != '/') $dir.= '/';
	//echo "list_directory($dir)\n";
	$di = scandir($dir);
	foreach ($di as $de)
	{
		if (($de=='.') || ($de=='..')) continue;
		$de = $dir . $de;

		if (is_link($de))
		{
			$stat = lstat($de);
			$data = s3sync_stat_meta($stat);
			$data['stat_hash'] = s3sync_stat_hash($data);
			$dir_list[$de] = $data;
		}
		elseif (is_dir($de))
		{
			list_directory($de);
		}
		elseif (is_file($de))
		{
			$stat = stat($de);
			$data = s3sync_stat_meta($stat);
			$data['stat_hash'] = s3sync_stat_hash($data);
			$dir_list[$de] = $data;
		}
		else
		{
			echo "Skipping $de (".filetype($de).")\n";
		}
	}
}

// func: local_shutdown_function()
function local_shutdown_function()
{
	global $s3c;
	$ram = memory_get_peak_usage(true);
	echo sprintf("Memory Used: %d B, %d KB, %d MB\n",$ram,$ram/1024,$ram/(1024*1024));
	print_r($s3c);
}

// func: s3sync_cache_update($o_bucket,$o_prefix)
function s3sync_cache_update($o_bucket,$o_prefix)
{
	global $s3c;
	$s3cache = new S3Cache("/opt/edoceo/var/s3sync/$o_bucket.cache");

	$delim = '//';
	$marker = null;
	$c_object = 0;

	do
	{
		echo "ls($o_prefix,$marker,$delim);\n";
		$s3r = $s3c->ls($o_prefix,$marker,$delim);
		if (!$s3r->is_success) die("Error: $s3r->error_code; $s3r->error_message\n");

		$ea = $s3r->entries;
		foreach ($ea as $e)
		{
			if ($e->type!='o') continue;

			$c_object++;

			// Display
			//print_r($e);
			$cache_name = $s3c->path_fix('/'.$e->name);
			echo "C?: $cache_name\n";

			// Perform stat on this Object then update Cache
			$s3stat = $s3c->stat($e->name);
			$s3meta = $s3stat->meta;
			$s3hash = s3sync_stat_hash($s3meta);
			//echo "S3Meta: ".print_r($s3meta,true);

			$mymeta = $s3cache->hit($cache_name);
			$myhash = s3sync_stat_hash($mymeta);
			//echo "MyMeta: ".print_r($mymeta,true);

			// If any Difference S3 is authority
			if ($myhash != $s3hash)
			{
				echo "C! Hash mismatch\n";
				$s3cache->add($cache_name,$s3meta);
			}
		}
		$marker = $s3r->is_truncated ? $s3r->next_marker : null;
	}
	while ($s3r->is_truncated);
	printf("%u entries checked/updated\n",$c_object);
	echo "Cache Update Finished";
}

// func: s3sync_stat_hash($stat,$prefix=null) - returns a hash sum of the metadata values
function s3sync_stat_hash($stat,$prefix=null)
{
	$buf = '/';
	foreach(array('ino','uid','gid','size','atime','ctime','mtime') as $x)
	{
		$k = $prefix.$x;
		if (isset($stat[$k])) $buf = $x.'='.$stat[$k].'/';
	}
	return md5($buf);
}

// func: s3sync_stat_meta($stat,$prefix=null) - returns array of stat-ish metadata
function s3sync_stat_meta($stat,$prefix=null)
{
	$meta = array();
	foreach(array('ino','uid','gid','size','atime','ctime','mtime') as $x)
	{
		$k = $prefix.$x;
		if (isset($stat[$k])) $meta[$x] = $stat[$k];
	}
	return $meta;
}
?>
