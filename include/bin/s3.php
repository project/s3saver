#!/usr/bin/php
<?php
// spec: reference implmentation of a command utility to work with S3, tests libs3.php

error_reporting(E_ALL | E_STRICT);
register_shutdown_function('local_shutdown_function');
define('S3_CONF','/opt/edoceo/etc/aws/s3.php');
putenv('TZ=America/Los_Angeles');


require_once('/opt/edoceo/lib/libs3.php');

// S3_CONF file 
$aws_key = null;
$aws_secret = null;
// The vars above can be overridden in S3_CONF
if (is_file(S3_CONF)) include_once(S3_CONF);

$options = array();
$options['command'] = null;
$options['verbose'] = getenv('verbose');
$options['recurse'] = false;

$paths = array();
array_shift($argv); // Get rid if my name
while ($a = array_shift($argv))
//for ($i=1;$i<$argc;$i++)
{
	switch ($a)
	{
	case '--verbose':
	case '-v':
		$options['verbose'] = true;
		$config['debug_trace'] = true;
		break;
	case '--pretend':
	case '-p':
		$options['pretend'] = true;
		break;
	case '--recursive':
	case '-r':
		$options['recurse'] = true;
		break;
	default:
		if (in_array($a,array('auth','get','ls','put','rm','stat','test')))
			$options['command'] = $a;
		else
			$paths[] = $a;
	}
}

if ($options['verbose']) echo "Options:".print_r($options,true);

$s3c = new S3Connection($aws_key,$aws_secret);

if ($options['command']=='auth')
{
	$url = $s3c->auth($paths[0]);
	echo "$url\n";
}
elseif ($options['command']=='get')
{
	$s3c->get($paths[0],$paths[1]);
}
elseif ($options['command']=='ls')
{
	$s3path = isset($paths[0]) ? $paths[0] : '/';
	$delim = '/';
	if ($options['recurse']) $delim = '//';
	$marker = null;
	$t_object_size = 0;
	$c_object = 0;

	do
	{
		$s3r = $s3c->ls($s3path,$marker,$delim);
		if (!$s3r->is_success) die("Error: $s3r->error_code; $s3r->error_message\n");

		$ea = $s3r->entries;
		foreach ($ea as $e)
		{
			// Display
			if ($e->type=='b') echo 'b--  ';
			elseif ($e->type=='p') echo '-p-  ';
			elseif ($e->type=='o')
			{
				echo '--o  ';
				$t_object_size += $e->size;
				$c_object++;
			}
			echo date('m/d/y h:i',strtotime($e->date));
			echo sprintf(' % 10db ',size_nice($e->size));
			echo "  $e->name\n";
		}
		$marker = $s3r->is_truncated ? $s3r->next_marker : null;
	}
	while ($s3r->is_truncated);
	echo sprintf("%u objects using %u bytes (%d MB)\n",$c_object,$t_object_size,($t_object_size/(1024*1024)));
	//if ($c_object > 1000) echo sprintf("%u entries\n%u bytes (%d MB)\n",$c_object,$t_object_size,($t_object_size/1024/1024));
}
elseif ($options['command']=='put')
{
	if (is_link($paths[0])) die("Cannot handle putting Symlinks\n");
	elseif (is_dir($paths[0])) die("Cannot handle putting Directories, put it's contents\n");
	elseif (is_file($paths[0]))
	{
		$s3c->put($paths[0],$paths[1]);
	}
	else die("Unexpected\n");
}
elseif ($options['command']=='rm')
{
  $s3r = $s3c->rm($paths[0]);
	if (!$s3r->is_success) die("Error: $s3r->as_string");
	echo "$s3r->as_string\n";
}
elseif ($options['command']=='stat')
{
  $s3r = $s3c->stat($paths[0]);
	if (!$s3r->is_success) die("Error: $s3r->as_string");
	echo "$s3r->as_string\n";
	print_r($s3r);
}
elseif ($options['command']=='test')
{
  // Bucket Name
	$bn = sprintf('%s.%u',tempnam('/tmp/','s3'),time());
	
	echo "Create Bucket\n";
	$s3r = $s3c->put($bn);
	echo $s3r->as_string."\n";
	
	echo "Create Empty Object\n";
	$s3r = $s3r->put(null,"/$bn/prefix-00/prefix/object.key");
	echo $s3r->as_string."\n";
	
	echo "Create Object from File\n";
	$s3r = $s3r->put('/etc/hosts',"/$bn/prefix-00/prefix/object.key");
	echo $s3r->as_string."\n";
	
	echo "List Bucket\n";
	$s3r = $s3c->ls($bn);
	echo $s3r->as_string."\n";
}
else
{
	echo <<<EOF
s3.php [options] command parameters

Options
	--pretend (-p)       Show what would happen
	--recursive (-r)     Operate Recursively
	--verbose (-v)       Be Verbose

Command
	Can be one of: auth, get, ls, put, rm, stat, test

EOF;
}
exit(0);

// func: local_shutdown_function()
function local_shutdown_function()
{
	$ram = memory_get_peak_usage(true);
	echo sprintf("Used: %s\n",size_nice($ram));
}

// func: size_nice($bytes)
function size_nice($bytes)
{
	$nice = array('B','KB','MB','GB');
	$c = count($nice);
	$i = 0;
	$size = $bytes;
	while(($size/1024)>1 && $i<$c)
	{
		 $size=$size/1024;
		 $i++;
	}
	return number_format($size,1).$nice[$i];
}

/*
// mcrypt utility functions *******************************************
// todo: how to decrypt without a prompt?
function mcrypt_decrypt_file($file)
{
  if (!is_readable($file)) die('Cannot decrypt unreadable file');
  $x = array(0=>array('pipe','r'), 1=>array('pipe','w'), 2=>array('pipe','w'));
  $cmd = "/usr/bin/mcrypt --algorithm rijndael-256 --hash sha256 --keymode s2k-salted-sha1 --mode cbc --openpgp --decrypt $file";
  $ph = proc_open($cmd, $x, $io);
  if (!is_resource($ph)) die('Failed to execute mcrypt');
  fwrite($io[0],"password\n");
  $ebuf=$obuf='';
  while (!feof($io[1])) $obuf.= fread($io[1],4096);
  while (!feof($io[2])) $ebuf.= fread($io[2],4096);
  fclose($io[0]);
  fclose($io[1]);
  fclose($io[2]);
  $x = proc_close($ph);
  if ($x != 0)
  {
    trigger_error("\n!! Unknown Return from mcrypt: $x\nOutput Buffer:\n$obuf\nError Buffer:\n$ebuf\n");
    return(null);
  }
  return $outfile;
}

function mcrypt_encrypt_file($file)
{
  if (!is_readable($file)) die('Cannot encrypt unreadable file');
  $x = array(0=>array('pipe','r'), 1=>array('pipe','w'), 2=>array('pipe','w'));
  $cmd = "/usr/bin/mcrypt --algorithm rijndael-256 --hash sha256 --keymode s2k-salted-sha1 --mode cbc --openpgp $file";
  $ph = proc_open($cmd, $x, $io);
  if (!is_resource($ph)) die('Failed to execute mcrypt');
  $ebuf=$obuf='';
  while (!feof($io[1])) $obuf.= fread($io[1],4096);
  while (!feof($io[2])) $ebuf.= fread($io[2],4096);
  fclose($io[0]);
  fclose($io[1]);
  fclose($io[2]);
  $x = proc_close($ph);
  if ($x != 0)
  {
    trigger_error("\n!! Unknown Return from mcrypt: $x\nOutput Buffer:\n$obuf\nError Buffer:\n$ebuf\n");
    return(null);
  }
  else return("$file.gpg");
}
*/
?>
