<?php
// spec: Library File for Amazon SQS and S3 Services

// from: http://www.intellispire.com/web/grid-computing/amazon-web-services/php-sqs-class-released.html

class AmazonSQS
{
  private $_access_key_id;
  private $_access_key_secret;

  function __construct($a,$s)
  {
    $this->_access_key_id = $a;
    $this->_access_key_secret = $s;
  }
  
  function list_queues()
  {
    
  }
  
  function push_message()
  {
    
  }
  
  function pop_message()
  {
    
  }

  function setActiveQueue($q)
  {
    $this->activeQueue = $q;
  }

  function ListQueues($QueueNamePrefix = '') {
     $params = array();
     if ($prefix != '') {
       $params['QueueNamePrefix'] = $QueueNamePrefix;
     }
     $result = $this->_call('ListQueues', $params);
     if ($this->statuscode != 'Success') return NULL;
     return $result['QUEUEURL'];
  }

  function CreateQueue($QueueName, $setActive = true, $DefaultVisibilityTimeout = '') {
     $params = array();
     if ($DefaultVisibilityTimeout != '') {
       $params['DefaultVisibilityTimeout'] = $DefaultVisibilityTimeout;
     }

     $params['QueueName'] = $QueueName;
     $result = $this->_call('CreateQueue', $params);
     $q = $result['QUEUEURL'];
     if ($this->statuscode != 'Success') return NULL;
     if ($q && $setActive) $this->activeQueue = $q;
     return $q;
  }   

  function DeleteQueue($QueueName)
  {
     $params = array();
     $oldActiveQueue = $this->activeQueue;
     $this->activeQueue = $QueueName;
     $result = $this->_call('DeleteQueue', $params);
     $this->activeQueue = $oldActiveQueue;
     if ($this->statuscode != 'Success') return false;
     return true;
  }

  function SendMessage($MessageBody )
  {
     $params = array();
     $params['MessageBody'] = $MessageBody;
     $result = $this->_call('SendMessage', $params, $this->activeQueue);
     if ($this->statuscode != 'Success') return NULL;
     return $result['MESSAGEID'];
  }

  // Returns 0 or 1 messages
  function ReceiveMessage($VisibilityTimeout = -1)
  {
    $params = array();
    if ($VisibilityTimeout > -1) $params['VisibilityTimeout'] = $VisibilityTimeout;
    $result = $this->_call('ReceiveMessage', $params, $this->activeQueue);
    if ($this->statuscode != 'Success') return NULL;
    return $result;
  }

  // Returns 0 or more messages, formatted an an array of messages
  function ReceiveMessages($NumberOfMessages = -1, $VisibilityTimeout = -1) {
    $params = array();
    if ($NumberOfMessages  > 0)  $params['NumberOfMessages'] = $NumberOfMessages;
    if ($VisibilityTimeout > -1) $params['VisibilityTimeout'] = $VisibilityTimeout;
    $result = $this->_call('ReceiveMessage', $params, $this->activeQueue);
    if ($this->statuscode != 'Success') return NULL;
 
    $id   = $result['MESSAGEID'];
    $body = $result['MESSAGEBODY'];
    $messages = array();

    if (is_array($id)) {
      $count = 0;
 
 
      for ($i = 0; $i < count($id); $i++) {
        $msg = array();
        $msg['MESSAGEID']   = $id[$i];
        $msg['MESSAGEBODY'] = $body[$i];
        $messages[] = $msg;
      }
    } else {
      // Only 1 message, but we want it as part of an array
      $messages[] = $result;
    }
    return $messages;
 
  }

  function DeleteMessage($MessageId)
  {
     $params = array();
     $params['MessageId'] = $MessageId;
     $result= $this->_call('DeleteMessage', $params, $this->activeQueue);
     if ($this->statuscode != 'Success') return false;
     return $this->true;
  }

  function PeekMessage($MessageId)
  {
     $params = array();
     $params['MessageId'] = $MessageId;
     $result = $this->_call('PeekMessage', $params, $this->activeQueue);
     if ($this->statuscode != 'Success') return NULL;
     return $result;
  }

 /**
   * Set queue visibility
   *
   * According to the docs, it is possible to set the visibility on a
   * per message basis.  The docs are inaccurate, it can only be done per queue.
   *
   */

  function SetVisibilityTimeout($VisibilityTimeout /*, $MessageId = '' */) {
    $params = array();
    // if ($MessageId != '') $params['MessageId'] = $MessageId;
    $params['VisibilityTimeout'] = $VisibilityTimeout;
    $this->_call('SetVisibilityTimeout', $params, $this->activeQueue);
    if ($this->statuscode != 'Success') return NULL;
    return $this->true;
  }

  function GetVisibilityTimeout() {
    $params = array();
    $result = $this->_call('GetVisibilityTimeout', $params, $this->activeQueue);
    if ($this->statuscode != 'Success') return NULL;
    return ($result['VISIBILITYTIMEOUT']);
  }

  // [RECEIVEMESSAGE, FULLCONTROL, SENDMESSAGE]
  function AddGrant($Grantee, $Permission = 'RECEIVEMESSAGE', $MessageId = '')
  {
    $params = array();
    $params['Grantee.EmailAddress'] = $Grantee;
    $params['Permission'] = $Permission;
    if ($MessageId != '') $params['MessageId'] = $MessageId;
    $result = $this->_call('AddGrant', $params, $this->activeQueue);
    if ($this->statuscode != 'Success') return NULL;
    return $this->errorcode;
   }
    
  function RemoveGrant($Grantee, $Permission = 'RECEIVEMESSAGE', $MessageId = '') {
    $params = array();
    $params['Grantee.EmailAddress'] = $Grantee;
    $params['Permission'] = $Permission;
    if ($MessageId != '') $params['MessageId'] = $MessageId;
    $result = $this->_call('RemoveGrant', $params, $this->activeQueue);
    if ($this->statuscode != 'Success') return NULL;
    return true;
  }
 
  // [RECEIVEMESSAGE, FULLCONTROL, SENDMESSAGE]
  function ListGrants($Grantee, $Permission = '') {
    $params = array();
    $params['Grantee.EmailAddress'] = $Grantee;
    if($Permission != '') $params['Permission'] = $Permission;
    $result = $this->_call('ListGrants', $params, $this->activeQueue);
    if ($this->statuscode != 'Success') return NULL;
    return $result;
  }
  
  function _call($action, &$params, $q = '') {
    if ($params == '') {
      $params = array();
    }
   
    // Add Actions
    $params['Action'] = $action;
    $params['Version'] = '2006-04-01';
    $params['AWSAccessKeyId'] = $this->access_key;
    $params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');  

    // Sign the string
    $string_to_sign = $params['Action'] . $params['Timestamp'];
    $hasher =& new Crypt_HMAC($this->secret, "sha1");
    $params['Signature'] =  $this->hex2b64($hasher->hash($string_to_sign));

    $result = $this->_call2($action, &$params, $q);
    return $result;
  }

  function _call2($action, $params, $q) {
    $request = '';
    foreach ($params as $name => $value) {
      $request .= $name . '=' .  urlencode($value) . '&';
    }
    $this->pchop($request);

    $endpoint = "http://queue.amazonaws.com/";
    if ($action == 'DeleteQueue') {
      $endpoint = $this->activeQueue;
    } else
    {
      if ( strpos($q, ':') === false ) {
        $endpoint .= $q;
      } else {
        $endpoint = $q;
      }
    }

    $req = & new HTTP_Request($endpoint);
    $req->setMethod('GET');
    $req->addRawQueryString($request);
    $req->sendRequest();

 
    $xml = $req->getResponseBody();

    $xml_parser = xml_parser_create();
    xml_parse_into_struct($xml_parser, $xml, $vals, $index);

    $xml_data = array();
    $name = '';
    $use_array = true;
    foreach ($vals as $val) {
      $name  = $val['tag'];
      $value = $val['value'];

      if ($value != '') {
         if ($xml_data[$name]) {
           // We already have a value here!
           $temp = $xml_data[$name];
           if (! is_array($temp)) {
             $xml_data[$name] = $array;
             $xml_data[$name][] = $temp;
           }    
           $xml_data[$name][] = $value;
         } else {
          $xml_data[$name] = $value;
         }
      } 
    }

    $this->resp_xml = $xml;

    $this->statuscode = $xml_data['STATUSCODE'];
    unset($xml_data['STATUSCODE']);
 
    $this->requestid  = $xml_data['REQUESTID'];
    unset($xml_data['REQUESTID']);

    // Normalize
    if ($xml_data['CODE']) {
      $this->statuscode = $xml_data['CODE'];
      unset($xml_data['CODE']);
    }

    $this->message  = $xml_data['MESSSAGE'];
    unset($xml_data['MESSAGE']);


    return $xml_data;

  }

  /**
   * implementation of Perl's "chop" function
   */
  function pchop(&$string) {
    if (is_array($string)) {
      foreach($string as $i => $val)
      {
        $endchar = $this->pchop($string[$i]);
      }
    } else {
      $endchar = substr("$string", strlen("$string") - 1, 1);
      $string = substr("$string", 0, -1);
    }
    return $endchar;
  }

  function hex2b64($str) {
    $raw = '';
    for ($i=0; $i < strlen($str); $i+=2) {
        $raw .= chr(hexdec(substr($str, $i, 2)));
    }
    return base64_encode($raw);
  }
}